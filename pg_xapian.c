
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "postgres.h"
#include "utils/array.h"
#include "catalog/pg_type.h"

#include "xapian_searcher.h"

#ifdef PG_MODULE_MAGIC
    PG_MODULE_MAGIC;
#endif

char* convert_text_to_char(text *source)
{
    size_t len = VARSIZE(source)-VARHDRSZ;
    char *out = (char *) palloc(len + 1);
    memcpy(out, VARDATA(source), len);
    out[len] = 0;
    return out;
}

Datum search_and_get_ids(
    text* raw_query,
    text* db_path,
    text* lang,
    int limit,
    int offset
) {
    //search and ids
    int* results = _search_and_get_ids(
        convert_text_to_char(raw_query),
        convert_text_to_char(db_path),
        convert_text_to_char(lang),
        limit,
        offset
    );

    // try to find length of result
    int i;
    int len = 0;
    for (i=0; i<limit; i++) {
        if (*(results + i) != 0) {
            len ++;
        } else {
            break;
        }
    }

    // init pg result array
    Datum * out = palloc(len * sizeof(Datum));
    
    for (i=0; i<len; i++) {
        *(out + i) = *(results + i);
    }
    ArrayType * pgarray = construct_array(out, len, INT4OID, 4, true, 'i');
    PG_RETURN_ARRAYTYPE_P(pgarray);
}

int search_and_get_count(
    text* raw_query,
    text* db_path,
    text* lang,
    int limit,
    int offset
) {
    return _search_and_get_count(
        convert_text_to_char(raw_query),
        convert_text_to_char(db_path),
        convert_text_to_char(lang),
        limit,
        offset
    );
}
