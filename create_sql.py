#!/usr/bin/env python

import os

tpl = """
CREATE OR REPLACE FUNCTION search_and_get_ids(text, text, text, int, int) RETURNS int[]
     AS '{folder}/pg_xapian', 'search_and_get_ids'
     LANGUAGE C STRICT;

CREATE OR REPLACE FUNCTION search_and_get_count(text, text, text, int, int) RETURNS int
     AS '{folder}/pg_xapian', 'search_and_get_count'
     LANGUAGE C STRICT;
"""

params = {
    "folder": os.path.split(os.path.abspath(__file__))[0],
}

with open('commands.sql', 'w') as f_obj:
    f_obj.write(tpl.format(**params))
