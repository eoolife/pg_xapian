
CC = gcc
CPPC = g++
XAPIAN_FLAGS = `xapian-config --libs` `xapian-config --cxxflags`
POSTGRES_FLAGS = -I `pg_config --includedir-server`
LD_FLAGS = -dynamic `xapian-config --libs`

all: clean pg_xapian.so commands.sql

clean:
	rm -rf *.o *.so commands.sql

xapian_searcher.o:
	$(CPPC) -c -fpic $(XAPIAN_FLAGS) xapian_searcher.cpp

pg_xapian.o:
	$(CC) -c -fpic $(POSTGRES_FLAGS) pg_xapian.c

pg_xapian.so: xapian_searcher.o pg_xapian.o
	$(CPPC) $(POSTGRES_FLAGS) -shared -o pg_xapian.so pg_xapian.o xapian_searcher.o $(LD_FLAGS)

commands.sql:
	python create_sql.py

test_db:
	python create_test_db.py