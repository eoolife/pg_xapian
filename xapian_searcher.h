#ifdef __cplusplus 
extern "C" { 
#endif

int* _search_and_get_ids(
    char* raw_query,
    char* db_path,
    char* lang,
    int limit,
    int offset
);

int _search_and_get_count(
    char* raw_query,
    char* db_path,
    char* lang,
    int limit,
    int offset
);

#ifdef __cplusplus 
}
#endif